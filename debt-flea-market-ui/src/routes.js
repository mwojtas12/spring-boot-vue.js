import homePage from './pages/HomePage.vue';
import loginPage from './login/LoginPage.vue';
import aboutUs from './pages/AboutUs.vue';
import resetPassword from './login/ResetPassword.vue';
import forgotPassword from './login/ForgotPassword.vue';
import restPasswordError from './login/ResetPasswordError.vue';
import restPasswordSent from './login/ResetLinkSent.vue';
import restPasswordSuccess from './login/ResetPasswordSuccess.vue';
import registerUser from './login/RegisterUser.vue';
import errorPage from './pages/Error.vue';
import dashboardPage from './pages/Dashboard.vue';

export default [
    {path: '/', component: homePage, meta: {allowAnonymous: true}},
    {path: '/about', component: aboutUs, meta: {allowAnonymous: true}},
    {path: '/login', component: loginPage, meta: {allowAnonymous: true}},
    {path: '/forgot', component: forgotPassword, meta: {allowAnonymous: true}},
    {path: '/reset/:token', component: resetPassword, meta: {allowAnonymous: true}},
    {path: '/error/reset', component: restPasswordError, meta: {allowAnonymous: true}},
    {path: '/forgot/sent', component: restPasswordSent, meta: {allowAnonymous: true}},
    {path: '/forgot/success', component: restPasswordSuccess, meta: {allowAnonymous: true}},
    {path: '/register', component: registerUser, meta: {allowAnonymous: true}},
    {path: '/errorPage', component: errorPage, meta: {allowAnonymous: true}},
    {path: '/dashboard', component: dashboardPage}
]