import axios from 'axios'


let stateHolder = {
    auth: {
        loggedIn: false,
        userDetails: {}
    },
    authTimer: {
        id: 0,
        isActive: false
    }
}

function clearAuthTimer() {
    if (stateHolder.authTimer.isActive) {
        clearInterval(stateHolder.authTimer.id);
        stateHolder.authTimer.isActive = false;
    }
}

function setAuthTimer() {
    clearAuthTimer();
    stateHolder.authTimer.id = setInterval(refreshTokens, 500 * 1000);
    stateHolder.authTimer.isActive = true;

}

function refreshTokens() {
    const refreshToken = window.localStorage.getItem('refresh_token');
    if (!refreshToken) {
        localStorage.removeItem('access_token');
        localStorage.removeItem("refresh_token");
        axios.defaults.headers.common['Authorization'] = '';
        return;
    }

    let params = new URLSearchParams();
    params.append('grant_type', 'refresh_token');
    params.append('refresh_token', refreshToken);
    return axios({
        method: 'post',
        url: 'oauth/token',
        auth: {username: 'my-trusted-client', password: 'secret'},
        headers: {"Content-type": "application/x-www-form-urlencoded; charset=utf-8"},
        data: params
    }).then((response) => {
        window.localStorage.setItem('access_token', response.data.access_token);
        window.localStorage.setItem('refresh_token', response.data.refresh_token);
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.access_token;
    }, () => {
        localStorage.removeItem('access_token');
        localStorage.removeItem("refresh_token");
        axios.defaults.headers.common['Authorization'] = '';
    });
}

export default {
    debug: true,
    state: stateHolder,
    clearAuthentication() {
        if (this.debug) console.log('clearAuthentication');
        this.state.auth.loggedIn = false;
        this.state.auth.userDetails = {};
        localStorage.removeItem('access_token');
        localStorage.removeItem("refresh_token");
        axios.defaults.headers.common['Authorization'] = '';
        clearAuthTimer();
    },
    setAuthentication(accessToken, refreshToken) {
        if (this.debug) console.log('set authentication as:', accessToken);
        if (accessToken && refreshToken) {
            localStorage.setItem('refresh_token', refreshToken);
            localStorage.setItem('access_token', accessToken);
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
            axios.get('/user').then(response => {
                this.state.auth.loggedIn = true;
                this.state.auth.userDetails = response.data;
                setAuthTimer();
                if (this.debug) console.log('authenticated as:', this.state.auth.loggedIn, this.state.auth.userDetails.username);
            }).catch(() => {
                this.clearAuthentication();
            });
        }
    },
    initializeAuthentication(onFailure) {
        if (this.debug) console.log("Initialize authentication");
        refreshTokens();
        if (localStorage.getItem('access_token')) {
            setAuthTimer();
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
            axios.get('/user').then(response => {
                this.state.auth.loggedIn = true;
                this.state.auth.userDetails = response.data;
                if (this.debug) console.log('authenticated as:', this.state.auth.loggedIn, this.state.auth.userDetails.username);
            }).catch(() => {
                this.clearAuthentication();
                if (onFailure) {
                    onFailure();
                }
            });
        } else if (onFailure) {
            onFailure();
            this.clearAuthentication();
        }
    },
};