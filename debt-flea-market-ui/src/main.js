import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import App from './App.vue'
import Routes from './routes'
import authStore from './authStore'

axios.defaults.baseURL = 'http://localhost:8080';

Vue.use(VueAxios, axios);
Vue.use(VueRouter);

const router = new VueRouter({
    routes: Routes,
    mode: 'history'
});

authStore.initializeAuthentication();
window.onfocus = function () {
    authStore.initializeAuthentication(function () {
        if (!router.history.current.meta.allowAnonymous) {
            router.push({path: '/'});
        }
    });
};

axios.interceptors.response.use(undefined, function (error) {
    if (401 === error.response.status) {
        router.push({path: '/login'});
    }
    return Promise.reject(error);
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => !record.meta.allowAnonymous)) {
        if (!authStore.state.auth.loggedIn) {
            next({
                path: '/login',
                query: {
                    redirect: to.path,
                },
            });
        } else {
            next();
        }
    } else {
        next();
    }
});

new Vue({
    el: '#app',
    render: h => h(App),
    router: router,
    data: authStore
});