package com.flea.market;

import com.flea.market.config.CustomUserDetails;
import com.flea.market.entities.Role;
import com.flea.market.entities.User;
import com.flea.market.model.Person;
import com.flea.market.repository.ReactivePersonRepository;
import com.flea.market.repository.UserRepository;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;
import java.util.Arrays;

@SpringBootApplication
public class WebApplication extends SpringBootServletInitializer implements CommandLineRunner {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(WebApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }

    private PasswordEncoder passwordEncoder;

    private ReactivePersonRepository personRepository;

    private UserRepository userRepository;

    @Autowired
    public WebApplication(PasswordEncoder passwordEncoder, ReactivePersonRepository personRepository, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.personRepository = personRepository;
        this.userRepository = userRepository;
    }

    @Autowired
    public void globalUserDetails(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(s -> new CustomUserDetails(userRepository.findByUsername(s).block())).passwordEncoder(passwordEncoder);
    }

    @Override
    public void run(String args[]) {

        final Person johnAoe = new Person("john", "aoe", LocalDateTime.now(), "loser", 0);
        final Person johnBoe = new Person("john", "boe", LocalDateTime.now(), "a bit of a loser", 10);
        final Person johnCoe = new Person("john", "coe", LocalDateTime.now(), "average", 100);
        final Person johnDoe = new Person("john", "doe", LocalDateTime.now(), "winner", 1000);
        personRepository.deleteAll().subscribe();
        personRepository.saveAll(Flux.just(johnAoe, johnBoe, johnCoe, johnDoe)).subscribe();

        final User user = new User("user@gmail.com", passwordEncoder.encode("password"), Arrays.asList(new Role("USER")));
        final User admin = new User("admin@gmail.com", passwordEncoder.encode("password"), Arrays.asList(new Role("ADMIN")));
        userRepository.deleteAll().subscribe();
        userRepository.saveAll(Flowable.just(user, admin)).subscribe();
    }
}
