package com.flea.market.model;

import lombok.*;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString(exclude = {"id", "dateOfBirth"})
@Document
@NoArgsConstructor
public class SomeItem {

    @Id
    @JsonIgnore
    private String id;
    private String firstName;
    private String lastName;
    private String profession;
    private int salary;

    public SomeItem(
            final String firstName,
            final String lastName,
            final LocalDateTime dateOfBirth,
            final String profession,
            final int salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.profession = profession;
        this.salary = salary;
    }
}
