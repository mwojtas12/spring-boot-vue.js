package com.flea.market.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString(exclude = {"id", "dateOfBirth"})
@Document
public class Person {

    @Id
    private String id;
    private String firstName;
    private String lastName;
    private LocalDateTime dateOfBirth;
    private String profession;
    private int salary;

    public Person(
            final String firstName,
            final String lastName,
            final LocalDateTime dateOfBirth,
            final String profession,
            final int salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.profession = profession;
        this.salary = salary;
    }
}
