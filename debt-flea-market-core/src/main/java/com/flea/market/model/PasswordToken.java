package com.flea.market.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Random;

@Getter
@Setter
@Document
public class PasswordToken {
    @Id
    private String id;
    private String token;
    private String username;
    private LocalDateTime date;


    public PasswordToken(String username) {
        this.username = username;
        this.token = generateToken();
        System.out.println(token);
        this.date = LocalDateTime.now();
    }

    private String generateToken() {
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 40; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }
}
