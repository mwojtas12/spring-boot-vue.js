package com.flea.market.handler;

import com.flea.market.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.Random;

@Service
public class UserHandler {
    @Autowired
    private UserRepository userRepository;


    private static String TOKEN = "123";

    public Mono<ServerResponse> resetPassword(ServerRequest request) {
        return ServerResponse.ok().body(Mono.just("Token: " + TOKEN), String.class);
    }

    public Mono<ServerResponse> getResetPasswordToken(ServerRequest request) {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }

        TOKEN = sb.toString();
        return ServerResponse.ok().body(Mono.just(TOKEN), String.class);
    }


}