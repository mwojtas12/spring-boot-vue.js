package com.flea.market.handler;

import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Service
public class RestHandler {

    public Mono<ServerResponse> getRestData(ServerRequest request) {
        return ServerResponse.ok().body(Mono.just("REST test"), String.class);
    }
}