package com.flea.market.repository;


import com.flea.market.model.Person;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Repository interface to manage {@link Person} instances.
 *
 * @author Mark Paluch
 */
public interface ReactivePersonRepository2 extends ReactiveCrudRepository<Person, String> {

    /**
     * Derived query selecting by {@code lastName}.
     *
     * @param lastName
     * @return
     */
    Flux<Person> findByLastName(String lastName);

    /**
     * String query selecting one entity.
     *
     * @param lastName
     * @return
     */
    @Query("{ 'firstName': ?0, 'lastName': ?1}")
    Mono<Person> findByFirstNameAndLastName(String firstName, String lastName);

    /**
     * Derived query selecting by {@code lastName}. {@code lastName} uses deferred resolution that does not require
     * blocking to obtain the parameter value.
     *
     * @param lastName
     * @return
     */
    Flux<Person> findByLastName(Mono<String> lastName);

    /**
     * Derived query selecting by {@code firstName} and {@code lastName}. {@code firstName} uses deferred resolution that
     * does not require blocking to obtain the parameter value.
     *
     * @param firstName
     * @param lastName
     * @return
     */
    Mono<Person> findByFirstNameAndLastName(Mono<String> firstName, String lastName);

    /**
     * Use a tailable cursor to emit a stream of entities as new entities are written to the capped collection.
     *
     * @return
     */
    Flux<Person> findWithTailableCursorBy();
}