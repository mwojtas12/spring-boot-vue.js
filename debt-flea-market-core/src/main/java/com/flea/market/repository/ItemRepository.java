package com.flea.market.repository;

import com.flea.market.model.SomeItem;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;


public interface ItemRepository extends ReactiveCrudRepository<SomeItem, Long> {
    Mono<SomeItem> findById(String id);

    Mono<Void> deleteById(String id);
}
