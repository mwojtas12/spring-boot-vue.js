package com.flea.market.repository;

import com.flea.market.model.Person;
import io.reactivex.Observable;
import io.reactivex.Single;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.reactive.RxJava2CrudRepository;

public interface RxJava2Repository extends RxJava2CrudRepository<Person, String> {


    Observable<Person> findByLastName(String lastName);


    @Query("{ 'firstname': ?0, 'lastname': ?1}")
    Single<Person> findByFirstNameAndLastName(String firstName, String lastName);


    Observable<Person> findByLastName(Single<String> lastName);

    Single<Person> findByFirstNameAndLastName(Single<String> firstName, String lastName);

    /**
     * Use a tailable cursor to emit a stream of entities as new entities are written to the capped collection.
     *
     * @return
     */
    Observable<Person> findWithTailableCursorBy();
}