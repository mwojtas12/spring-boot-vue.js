package com.flea.market.repository;

import com.flea.market.model.PasswordToken;
import com.sun.org.apache.xpath.internal.operations.Bool;
import io.reactivex.Single;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface PasswordTokenRepository extends ReactiveCrudRepository<PasswordToken, String> {
    Mono<PasswordToken> findByToken(final String token);

    Single<Bool> existsBy(final String token);
}