package com.flea.market.entities.users;

import io.netty.util.internal.StringUtil;
import lombok.Data;

@Data
public class PasswordReset {
    private String password;
    private String passwordConfirmation;

    public boolean isValid() {
        return !StringUtil.isNullOrEmpty(password) && !StringUtil.isNullOrEmpty(passwordConfirmation) && password.equals(passwordConfirmation);
    }
}
