package com.flea.market.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@ToString(exclude = {"id", "username", "roles"})
@NoArgsConstructor
@Document
public class User {

    @Id
    private String id;
    private String username;

    @JsonIgnore
    private String password;
    private List<Role> roles;
    
    public User(String username, String password, List<Role> roles) {
        this.username = username;
        this.password = password;
        this.roles = roles;
    }
}
