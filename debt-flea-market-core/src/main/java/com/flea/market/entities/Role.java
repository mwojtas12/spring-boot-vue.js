package com.flea.market.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@Document
public class Role {


    @Id
    private String id;
    String name;

    public Role(String name) {
        this.name = name;
    }
}
