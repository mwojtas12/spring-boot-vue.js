package com.flea.market.entities.users;

import lombok.Data;

@Data
public class UserRegistration {
    private String username;
    private String password;
    private String passwordConfirmation;
}
