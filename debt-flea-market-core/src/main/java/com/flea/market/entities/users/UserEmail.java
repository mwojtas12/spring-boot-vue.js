package com.flea.market.entities.users;

import lombok.Data;

@Data
public class UserEmail {
    private String email;
}
