/*
 * Copyright 2002-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flea.market;

import com.flea.market.handler.RestHandler;
import com.flea.market.handler.UserHandler;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter;
import org.springframework.http.server.reactive.ServletHttpHandlerAdapter;
import org.springframework.web.reactive.function.server.RouterFunction;
import reactor.ipc.netty.http.server.HttpServer;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.*;


//Try to implement router function - not working yet
public class Server {

	private static final String HOST = "localhost";

	private static final int PORT = 8090;


	@Autowired
	private UserHandler userHandler;

	@Autowired
	private RestHandler restHandler;

	public static void main(String[] args) throws Exception {
		Server server = new Server();
//		server.startReactorServer();
		server.startTomcatServer();

		System.out.println("Press ENTER to exit.");
		System.in.read();
	}

	@Bean
	public RouterFunction<?> routerFunction() {
		return route(POST("/user/forgot").and(accept(MediaType.APPLICATION_JSON)), userHandler::getResetPasswordToken)
				.and(route(POST("/user/reset/{token}").and(accept(MediaType.APPLICATION_JSON)), userHandler::resetPassword))
				.and(route(GET("/rest").and(accept(MediaType.APPLICATION_JSON)), restHandler::getRestData));
	}

	public void startReactorServer() throws InterruptedException {
		RouterFunction<?> route = routerFunction();
		HttpHandler httpHandler = toHttpHandler(route);

		ReactorHttpHandlerAdapter adapter = new ReactorHttpHandlerAdapter(httpHandler);
		HttpServer server = HttpServer.create(HOST, PORT);
		server.newHandler(adapter).block();
	}

	public void startTomcatServer() throws LifecycleException {
		RouterFunction<?> route = routerFunction();
		HttpHandler httpHandler = toHttpHandler(route);

		Tomcat tomcatServer = new Tomcat();
		tomcatServer.setHostname(HOST);
		tomcatServer.setPort(PORT);
		Context rootContext = tomcatServer.addContext("", System.getProperty("java.io.tmpdir"));
		ServletHttpHandlerAdapter servlet = new ServletHttpHandlerAdapter(httpHandler);
		Tomcat.addServlet(rootContext, "httpHandlerServlet", servlet);
		rootContext.addServletMapping("/", "httpHandlerServlet");
		tomcatServer.start();
	}

}
