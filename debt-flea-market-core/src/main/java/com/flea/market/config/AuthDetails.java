package com.flea.market.config;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuthDetails {
    private String username;
    private String authToken;
}