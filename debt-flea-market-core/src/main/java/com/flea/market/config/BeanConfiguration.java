package com.flea.market.config;

import com.flea.market.handler.RestHandler;
import com.flea.market.handler.UserHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.web.reactive.function.server.RouterFunction;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class BeanConfiguration extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private UserHandler userHandler;

    @Autowired
    private RestHandler restHandler;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public RouterFunction<?> routerFunction() {
        return route(POST("/user/forgot").and(accept(MediaType.APPLICATION_JSON)), userHandler::getResetPasswordToken)
                .and(route(POST("/user/reset/{token}").and(accept(MediaType.APPLICATION_JSON)), userHandler::resetPassword))
                .and(route(GET("/rest").and(accept(MediaType.APPLICATION_JSON)), restHandler::getRestData));
    }


}
