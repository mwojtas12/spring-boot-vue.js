package com.flea.market.controllers;

import com.flea.market.config.AuthDetails;
import com.flea.market.config.CustomUserDetails;
import com.flea.market.entities.Role;
import com.flea.market.entities.User;
import com.flea.market.entities.users.PasswordReset;
import com.flea.market.entities.users.UserEmail;
import com.flea.market.entities.users.UserRegistration;
import com.flea.market.model.PasswordToken;
import com.flea.market.repository.PasswordTokenRepository;
import com.flea.market.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

@org.springframework.web.bind.annotation.RestController
public class UserController {

    private PasswordTokenRepository passwordTokenRepository;
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(PasswordTokenRepository passwordTokenRepository, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.passwordTokenRepository = passwordTokenRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }


    @PostMapping("/user/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        if (authentication != null) {
            new SecurityContextLogoutHandler().logout(request, response, authentication);
        }
    }

    @GetMapping("/user")
    public Mono<ResponseEntity> getUserFromContext(Authentication authentication) {
        if (authentication == null) {
            return Mono.just(ResponseEntity.badRequest().build());
        }
        AuthDetails authDetails = new AuthDetails(((CustomUserDetails) authentication.getPrincipal()).getUsername(), ((OAuth2AuthenticationDetails) authentication.getDetails()).getTokenValue());
        return Mono.just(ResponseEntity.ok().body(authDetails));
    }

    @PostMapping("/user/reset/{token}")
    public Mono<ResponseEntity<User>> reset(@RequestBody PasswordReset passwordReset, @PathVariable String token) {
        if (passwordReset.isValid()) {
            return passwordTokenRepository.findByToken(token).
                    flatMap(u -> userRepository.findByUsername(u.getUsername())).flatMap(u -> {
                u.setPassword(passwordEncoder.encode(passwordReset.getPassword()));
                return userRepository.save(u);
            }).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.badRequest().build());
        }
        return Mono.just(ResponseEntity.badRequest().build());
    }

    @PostMapping("/user/forgot")
    public Mono<ResponseEntity<PasswordToken>> forgotten(@RequestBody UserEmail email) {
        return userRepository.findByUsername(email.getEmail()).
                flatMap(u -> passwordTokenRepository.save(new PasswordToken(u.getUsername())))
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.badRequest().build());
    }

    @PostMapping("/user/register")
    public Mono<ResponseEntity> register(@RequestBody UserRegistration userRegistration) {
        if (userRegistration.getUsername().contains("@") && userRegistration.getPassword().equals(userRegistration.getPasswordConfirmation())) {
            User user = new User(userRegistration.getUsername(), passwordEncoder.encode(userRegistration.getPassword()), Arrays.asList(new Role("USER")));
            userRepository.save(user).subscribe();
            return Mono.just(ResponseEntity.ok().build());
        }
        return Mono.just(ResponseEntity.badRequest().build());
    }
}
