package com.flea.market.controllers;

import io.reactivex.Observable;
import org.springframework.web.bind.annotation.GetMapping;
import reactor.core.publisher.Mono;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    @GetMapping("/rest/reactor")
    public Mono<String> reactor() {
        return Mono.fromSupplier(() -> "wtf reactor");
    }

    @GetMapping("/rest/rx2")
    public Observable<String> rx() {
        return Observable.just("WTF RX");
    }

    @GetMapping("/rest")
    public Observable<String> rest() {
        return Observable.just("WTF RX");
    }
}
