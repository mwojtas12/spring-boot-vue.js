package com.flea.market.controllers;

import io.reactivex.Observable;
import org.springframework.web.bind.annotation.GetMapping;

@org.springframework.web.bind.annotation.RestController
public class ProtectedController {

    @GetMapping("/protected")
    public Observable<String> rest() {
        return Observable.just("Protected resource");
    }
}
