package com.flea.market.controllers;

import com.flea.market.repository.ReactivePersonRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import reactor.core.publisher.Flux;

@org.springframework.web.bind.annotation.RestController
class PersonController {

    private final ReactivePersonRepository people;

    public PersonController(ReactivePersonRepository people) {
        this.people = people;
    }

    @GetMapping("/people")
    Flux<String> namesByLastname(@RequestParam String lastname) {

        return people
                .findByFirstName("john")
                .log()
                .map(p -> p.getLastName() + "  ");
    }
}