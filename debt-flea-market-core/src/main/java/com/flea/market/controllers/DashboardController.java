package com.flea.market.controllers;

import com.flea.market.model.SomeItem;
import com.flea.market.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@org.springframework.web.bind.annotation.RestController
public class DashboardController {

    private ItemRepository itemRepository;

    @Autowired
    public DashboardController(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @GetMapping("/dashboard/reset")
    public ResponseEntity<Object> test() {
        itemRepository.deleteAll().subscribe();
        return ResponseEntity.ok().build();
    }

    @GetMapping("/dashboard/items")
    public Flux<SomeItem> getItems() {
        return itemRepository.findAll();
    }

    @PostMapping("/dashboard/items")
    public Mono<SomeItem> addItem(@RequestBody SomeItem item) {
        return itemRepository.save(item);
    }

    @GetMapping("/dashboard/items/{id}")
    public Mono<SomeItem> getItem(@PathVariable String id) {
        return itemRepository.findById(id);
    }

    @DeleteMapping("/dashboard/items/{id}")
    public ResponseEntity<Object> deleteItem(@PathVariable String id) {
        itemRepository.deleteById(id).subscribe();
        return ResponseEntity.ok().build();
    }
}
